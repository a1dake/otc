import os
import sys
from datetime import datetime
import cfscrape
import requests
import urllib3

currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "otc.txt")  # путь к БД
sys.path.append(db_path)
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB


class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_json()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def add_order_to_db(self, numberOrder):
        try:
            with open(db_path, "w") as file:
                file.write(str(numberOrder))
        except Exception as e:
            print(e)

    def get_last_order(self):
        try:
            with open(db_path, "r") as file:
                order = file.read()
        except:
            order = 0
        return order

    def get_json(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        last_id = self.get_last_order()

        contents = []

        session = requests.Session()
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.46'
        })
        session.get(
            'https://otc.ru/tenders?searchform.state=1&searchform.documentsearchenabled=true&searchform.organizationlevels=commercial&searchform.organizationlevels=small',
            verify=False)

        main_url = 'https://otc.ru/microservices-otc/order/api/order/Search'
        json_data = {
            "filter_settings": {"isPersonal": True, "state": 1, "keywords_list": [], "exclude_keywords_list": [],
                                "universal_region": None, "delivery_city": None, "delivery_region": None, "okpd2": None,
                                "is_doc_search": True, "purchase_methods": None, "currency_code": None,
                                "prepayment": None, "has_applications": None,
                                "organization_levels": ["Commercial", "Small"],
                                "isApplicationWithoutSignatureAllowed": False, "organizationsTags": [], "is_msp": None,
                                "sales_channels": [], "region": None},
            "page_settings": {"sorting_field": "DatePublished", "sorting_direction": "Desc", "page_size": 2000,
                              "page_index": 1, "not_get_content_highlights": True}, "saveToHistory": False,
            "reportName": None}
        response = session.post(main_url, json=json_data, verify=False)
        data = response.json()

        item = 0
        for list in data['Result']['Tenders']:
            item += 1
            number_order = list.get('Id')

            if int(last_id) == int(number_order):
                break
            else:
                if item == 1:
                    self.add_order_to_db(number_order)

                item_data = {
                    'type': 2,
                    'title': '',
                    'purchaseNumber': '',
                    'fz': 'Коммерческие',
                    'purchaseType': '',
                    'url': '',
                    'lots': [],
                    'procedureInfo': {
                        'endDate': '',
                    },

                    'customer': {
                        'fullName': '',
                        'factAddress': '',
                        'inn': 0,
                        'kpp': 0,
                    }
                }

                item_data['title'] = str(self.get_title(list))
                item_data['purchaseType'] = str(self.get_purchase_type(list))
                item_data['purchaseNumber'] = str(self.get_purchase_number(list))
                item_data['customer']['fullName'] = str(self.get_full_name(list))

                item_data['customer']['inn'] = int(self.get_inn(list))
                item_data['customer']['kpp'] = int(self.get_kpp(list))

                item_data['customer']['factAddress'] = str(self.get_fact_address(list))
                item_data['url'] = str(list.get('LotUrl'))
                item_data['procedureInfo']['endDate'] = self.get_end_date(list)

                item_data['lots'] = self.get_tender_items(list)

                contents.append(item_data)
        return contents

    def get_title(self, data):
        try:
            title = ' '.join(str(data['LotName']).strip().split())
        except:
            title = ''
        return title

    def get_purchase_type(self, data):
        try:
            type = data['PurchaseMethod']
        except:
            type = ''
        return type

    def get_purchase_number(self, data):
        try:
            number = data['TenderId']
        except:
            number = ''
        return number

    def get_full_name(self, data):
        try:
            name = data['Customers'][0]['Name']
        except:
            name = ''
        return name

    def get_inn(self, data):
        try:
            inn_data = data['Customers'][0]['Inn']
            if '*****' in inn_data:
                inn = 0
            else:
                if not inn_data or inn_data is None:
                    inn = 0
                else:
                    inn = inn_data
        except:
            inn = 0
        return inn

    def get_kpp(self, data):
        try:
            kpp_data = data['Customers'][0]['Kpp']
            if '*****' in kpp_data:
                kpp = 0
            else:
                if not kpp_data or kpp_data is None:
                    kpp = 0
                else:
                    kpp = kpp_data
        except:
            kpp = 0
        return kpp

    def get_fact_address(self, data):
        try:
            adress = data['Customers'][0]['KladrRegionName']
        except:
            adress = ''
        return adress

    def get_end_date(self, data):
        try:
            date = data['ApplicationEndDate']
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date

    def get_tender_items(self, data):
        try:
            lots_data = []
            if len(data.get("TenderItems")) > 0:
                lots = {
                    'region': '',
                    'address': '',
                    'price': '',
                    'lotItems': []
                }
                for k in data.get("TenderItems"):
                    names = {'code': str(k.get("Number")), 'name': ''}
                    name = ' '.join(str(k.get("Name")).split())
                    if name is None:
                        names['name'] = ''
                    else:
                        names['name'] = name
                    lots['lotItems'].append(names)
                    price = lots['price'] = str(data.get("PriceNumber"))
                    if price is None:
                        lots['price'] = ''
                    adress = lots['address'] = str(data.get("DeliveryAddress")) or str(data.get("KladrDeliveryRegionName"))
                    if adress is None:
                        lots['address'] = ''
                    lots['region'] = str(data.get("KladrRegionName"))
                lots_data.append(lots)
        except:
            return []
        return lots_data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%Y-%m-%dT%H:%M:%S')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
